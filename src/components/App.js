import React from 'react';
import { Button, Drawer, Alert, GlobalStyles, defaultTheme, CGLogo, GitLabLogo, Spinner } from '@commonground/design-system';
import { ThemeProvider } from 'styled-components';
import Map from './Map';
import '../css/main.css';
import LocationNav from './LocationNav';
import MapStyleDark from '../data/mapstyle_osm_dark.json';
import MapStyleLight from '../data/mapstyle_osm_light.json';
import List from './List';
import { ReactComponent as Legend } from '../data/legenda.svg';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mapLoading: true,
      mapViewState: {
        center: [4.88, 52.35],
        zoom: 12,
        bearing: 0,
        pitch: 0
      },
      showDrawer: false,
      mapThemes: {
        "Donker": MapStyleDark,
        "Licht": MapStyleLight
      },
      mapStyle: "Licht",
      timeStamp: "",
      data: {},
      arrayMapInfo: {
        info: []
      },
      pointID: ""
    }
  };

  // Show drawer
  setShowDrawer = (v) => {
    this.setState({ showDrawer: v })
  }

  // Change status to false while map is loading to show loading icon
  isMapLoading = status => {
    this.setState({ mapLoading: status });
  };

  // Function to set the new view of the map after zooming or dragging
  changeMapViewState = (coords, zoom) => {
    this.setState({
      mapViewState: Object.assign({}, this.state.mapViewState, {
        center: coords,
        zoom: zoom
      })
    });
  };
  // Toggle light datrk mode map
  toggleMapTheme = () => {
    this.setState({
      mapStyle: this.state.mapStyle === "Licht" ? "Donker" : "Licht"
    });
  };
  // Get point info in map view
  setArrayMapInfo = (info) => {
    this.setState({
      arrayMapInfo: Object.assign({}, this.state.arrayMapInfo, {
        info: info
      })
    })
  };
  // Get all data from server
  getData = () => {
    // let uname = prompt("username " );
    // let pssw = prompt("password");
    //fetch(`http://127.0.0.1:8000/popularplaces?timestamp=1587393286` , {
    fetch(`https://covid19.api.commondatafactory.nl/popularplaces` , {
      credentials: "include"
    }).then(response => response.json().then(data => {
      // console.log(data)
      let date = new Date(data.scraped_at * 1000);
      this.setState({ timeStamp: date.toDateString() + " om " + date.toLocaleTimeString('en-US') })
      this.setState({ data: data })
    })
    )
      .catch((error) => {
        console.error('Error:', error);
      });
  };
  setPointID = (id) => {
    this.setState({
      pointID: id
    });
  }

  handleWindowResize = () => {
    this.setState({ isMobile: window.innerWidth <= this.state.breakpoints.md })
  }

  appHeight = () => {document.documentElement.style.setProperty('--app-height', `${window.innerHeight}px`)}
  appWidth = () => {document.documentElement.style.setProperty('--app-width', `${window.innerWidth}px`)}

  componentDidMount() {
    this.getData();
    window.addEventListener('resize', this.appHeight)
    this.appHeight();
    window.addEventListener('resize', this.appWidth)
    this.appWidth();
  }

  render() {
    return (
      <div className="App"
      // style={{ pointerEvents: this.state.mapLoading ? 'none' : "" }}
      >
        <ThemeProvider theme={defaultTheme}>
          <GlobalStyles />
          <div className="header">
            <h1>De Drukte Kaart <span>   {/* Show map loader */}
              {this.state.mapLoading ? (<Spinner className="spinner" >
              </Spinner>) : ""}</span> </h1>

            <h2 style={{ color: '#FE636D' }} >Data van {this.state.timeStamp} </h2>
          </div>

          <div className="panel">

            <List arrayMapInfo={this.state.arrayMapInfo}> </List>

            <h4>Legenda</h4>
            <Legend />

            {/* When mouse over point show graph of weekdays */}
            {/* {this.state.pointID ? (<Graph pointID={this.state.pointID} />) : ""} */}
            {/* Show top 5 most busy places in table  */}
          </div>
          <LocationNav
            changeMapViewState={this.changeMapViewState}
          />
          <Map
            mapViewState={this.state.mapViewState}
            isMapLoading={this.isMapLoading}
            mapStyle={this.state.mapThemes[this.state.mapStyle]}
            setArrayMapInfo={this.setArrayMapInfo}
            geoJsonData={this.state.data}
            setPointID={this.setPointID}
          />
          <div className="buttons">
            <Button className="button-general" onClick={() => this.setShowDrawer(true)}>Meer info</Button>
            <Button className="button-maptheme" onClick={this.toggleMapTheme}>Kaart Stijl: {this.state.mapStyle}</Button>
          </div>
          {this.state.showDrawer ? (
            <Drawer className="drawer"
              closeHandler={() => this.setShowDrawer(false)}
              autoFocus>
              <h1>Toelichting Over de drukte kaart</h1>
              <p>De gele cirkel laat de gemiddelde drukte op het huidige tijdstip van een plaats zien.</p>
            <Legend />
              <p>De groene of rode cirkel laat de huidige drukte zien. Is de circkel rood, dan is het drukker dan dat het gemiddeld is rond dit tijdstip.
                Is de cirkel groen, dan is het rustiger dan gemiddeld op dit tijdstip.
              </p>
              <p>Over het algemeen geld:</p>
              <h3>Hoe groter de cirkel, hoe drukker de plek op dit moment is.</h3>

              <h3>Wat kunnen we met de data?</h3>
              <p>
                Voor de locaties kunt u ieder uur zien hoe druk het er is, t.o.v. de gemiddelde drukte in een normale week. Deze kaart kan gebruikt worden als hulpmiddel bij het bepalen van prioriteit voor beoordeling ter plaatse, inzet handhaving en/of te nemen maatregelen.</p>

              <p>
                De data komt van Google, dit is dezelfde data die op Google Maps te zien is als ‘Populaire tijden’. Google bepaald dit op basis van gegevens die zij hebben over locatie en gebruik van telefoons en apps. Of zoals ze het zelf formuleren: “Bij de bepaling van populaire tijden, wachttijden en bezoekduur maakt Google gebruik van cumulatieve en geanonimiseerde gegevens van gebruikers die zich hebben aangemeld voor locatiegeschiedenis van Google” <a href="https://support.google.com/business/answer/6263531?hl=nl"> > bron</a>
              </p>
               {/* References */}
            <p>This application was made by: </p>
            <CGLogo style={{ height: '50px' }} />
            <p>
              <a target="blank" href="https://commonground.nl/">
                Build @ dev.loer Commonground.nl{" "}
              </a>
            </p>
            <p>
              <GitLabLogo style={{ height: '50px' }} />
              <a target="blank" href="https://gitlab.com/commondatafactory/">
                Code @ gitlab{" "}
              </a>
            </p>
            </Drawer>
          ) : ""}
        </ThemeProvider>
      </div>
    );
  };
};

export default App;
