import React from 'react';
import * as d3 from 'd3';

class Graph extends React.Component {
    state = {
        width: 400,
        height: 300,
        data: {},
        margin: ({ top: 60, right: 40, bottom: 30, left: 80 })
    }
    getData = (id) => {
        fetch(`https://covid19.api.commondatafactory.nl/place/${id}`, {
            headers: {
                'Authorization': 'Basic ' + btoa(':'),
                'Content-Type': 'application/json'
            }
        })
            .then(response => (response.ok ? response : Promise.reject(response)))
            .then(response => response.json().then(data => {
                this.prepareData(data)
            }));
    };
    prepareData = (data) => {
        let dates = Array.from(Array(24).keys());
        let series = data.populartimes;
        let correctData = {
            hours: dates,
            series: series
        }
        this.setState({ data: correctData } , () =>  this.drawChart()
        )
    }
    drawChart = () => {
        let x = d3.scaleTime()
            .domain(d3.extent(this.state.data.hours))
            .range([this.state.margin.left, this.state.width - this.state.margin.right])

        let y = d3.scalePoint()
            .domain(this.state.data.series.map(d => d.name))
            .range([this.state.margin.top, this.state.height - this.state.margin.bottom ])

        let z = d3.scaleLinear()
            .domain([0, d3.max(this.state.data.series, d => d3.max(d.data))]).nice()
            .range([0, -2 * y.step()])

        let xAxis = g => g
            .attr("transform", `translate(0,${this.state.height - this.state.margin.bottom})`)
            .call(d3.axisBottom(x)
                .ticks(12)
                .tickFormat(d3.timeFormat("%I %p"))
                .tickSizeOuter(0));

        let yAxis = g => g
            .attr("transform", `translate(${this.state.margin.left},0)`)
            .call(d3.axisLeft(y).tickSize(0).tickPadding(4))
            .call(g => g.select(".domain").remove());

        let area = d3.area()
            .curve(d3.curveBasis)
            .defined(d => !isNaN(d))
            .x((d, i) => x(this.state.data.hours[i]))
            .y0(0)
            .y1(d => z(d))

        let line = area.lineY1();

        const svg = d3.select(this.refs.myD3Div)
            .append("svg")
            .attr("width", this.state.width)
            .attr("height", this.state.height);

        svg.append("g")
            .call(xAxis)
        svg.append("g")
            .call(yAxis)

        const group = svg.append("g")
            .selectAll("g")
            .data(this.state.data.series)
            .join("g")
            .attr("transform", d => `translate(0,${y(d.name) + 1})`);

        // group.append("path")
        //     .attr("fill", "#ddd")
        //     .attr("opacity", 0.8)
        //     .attr("d", d => area(d.data));

        group.append("path")
            .attr("fill", "none")
            .attr("stroke", "#0B71A1")
            .attr("d", d => line(d.data));

        // let sampleData = this.state.data.slice(0,5)
        // this.setState({data: sampleData});
    }
    componentDidMount() {
        this.getData(this.props.pointID)
    }
    componentDidUpdate() {
        const svg = d3.select(this.refs.myD3Div).selectAll('svg');

        svg.selectAll("rect").remove();

        // svg.selectAll("rect")
        //     .data(this.props.data[0].populartimes)
        //     .enter()
        //     .append("rect")
        //     .attr("width", this.state.width / 7)
        //     .attr("height", (d, i) => d.data / 2)
        //     .attr("fill", "white")
        //     .attr("x", (d, i) => i * this.state.width / this.props.data.length)
        //     .attr("y", 0);
    }
    render() {
        return <div ref="myD3Div"></div>
    }
}

export default Graph;
