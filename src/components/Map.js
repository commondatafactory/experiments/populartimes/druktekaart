import React from 'react';
import mapboxgl from 'mapbox-gl';
import '../css/mapbox-gl-v1-9-1.css';
import './Map.css';

class Map extends React.Component {
    constructor(props) {
        super(props);
        this.mapRef = React.createRef();
    }
    state = {
        cirleColor: "#ffbc2c"
    };
    componentDidMount() {
        // MAP
        this.map = new mapboxgl.Map({
            container: this.mapRef.current, //react-reference toevoegen
            style: this.props.mapStyle,
            zoom: this.props.mapViewState.zoom,
            center: this.props.mapViewState.center,
            hash: true,
            attributionControl: false,
            maxZoom: 22
        });
        // Map Loading
        this.map.on("render", (e) => {
            if (e) {
                this.props.isMapLoading(true)
            }
        })
        this.map.on("idle", (e) => {
            if (e) {
                this.props.isMapLoading(false)
            }
        });

        // CONTROLS
        let nav = new mapboxgl.NavigationControl();
        this.map.addControl(nav, "top-left");
        // Scale bar control
        let scale = new mapboxgl.ScaleControl({
            maxWidth: 180,
            unit: "metric"
        });
        this.map.addControl(scale);

        // Attribution control
        let attr = new mapboxgl.AttributionControl({
            compact: true,
            customAttribution:
                "Made by: <a href='https://commondatafactory.nl/'> CDF</a>"
        });
        this.map.addControl(attr);

        // Mouse over point : mouse pointer on Circle 
        this.map.on("mouseover", "drukte", e => {
            this.map.getCanvas().style.cursor = e.features.length ? "pointer" : "";
            this.makePopup(e);
            this.props.setPointID("ChIJZVQ3a4AKxkcRYT5t2_6Ipm4")
        });
        // Mouse off point
        this.map.on('mouseleave', 'drukte', e => {
            this.map.getCanvas().style.cursor = '';
            this.popup.remove();
            // this.props.setPointID("")
        });

        // Add Data
        this.map.once("load", () => {
            this.addCircles();
            this.map.on('sourcedata', () => {
                if (this.map.getLayer('drukte')) {
                    let info = this.map.queryRenderedFeatures({ layers: ["drukte"] });
                    this.props.setArrayMapInfo(info);
                }

            })
        });

        // Get feature info in map window to app
        // this.map.on("move", (e) => {
        //     let info = this.map.queryRenderedFeatures({ layers: ["drukte"] });
        //     this.props.setArrayMapInfo(info)
        // })
    };

    makePopup = (e) => {
        // make POPUP 
        this.popup = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false,
            closeOnMove: true
        });
        // Set popup when features
        let features = e.features;
        if (features.length >= 1) {
            let feature = e.features[0];
            this.popup.setLngLat(feature.geometry.coordinates.slice());
            this.popup
                .setHTML(`<h4> Huidige drukte: ${feature.properties.current_popularity}  </h4> 
                <h4> Gemiddelde drukte: ${feature.properties.avg_p}  </h4> 
                <h4> Verschil: ${feature.properties.current_popularity - feature.properties.avg_p} </h4> 
                <p>${feature.properties.name} </p>
                <p id="small">${feature.properties.address} </p>`)
                .addTo(this.map);
        };
    };

    addCircles = () => {
        // Add custom data
        this.map.addLayer({
            id: 'drukte',
            source: {
                type: 'geojson',
                data: this.props.geoJsonData
            },
            type: 'circle',
            paint: {
                "circle-color": ["case", [">=", ["get", "current_popularity"], ["get", "avg_p"]], "#FE636D", "#39870C"],
                "circle-radius": ["interpolate", ["linear"], ["zoom"],
                8,5,
                    11, ["/", ["get", "current_popularity"], 5],
                    14, ["get", "current_popularity"],
                    18, ["*", ["get", "current_popularity"], 2]
                ],
                "circle-opacity": 0.5,
                "circle-stroke-color": ["case", [">=", ["get", "current_popularity"], ["get", "avg_p"]], "#FE636D", "#39870C"],
                "circle-stroke-opacity": 1,
                "circle-stroke-width": 1
            },
            layout: {
                // Mapbox Style Specification layout properties
            }
        });
        this.map.addLayer({
            id: 'drukte_avg',
            source: {
                type: 'geojson',
                data: this.props.geoJsonData
            },
            type: 'circle',
            paint: {
                "circle-color": "#ffbc2c",
                "circle-radius": ["interpolate", ["linear"], ["zoom"],
                    8, 1,
                    11, ["/", ["get", "avg_p"], 5],
                    14, ["get", "avg_p"],
                    18, ["*", ["get", "avg_p"], 2]
                ],
                "circle-opacity": 0,
                "circle-stroke-color": "#ffbc2c",
                "circle-stroke-opacity": 1,
                "circle-stroke-width": 3.5
            },
            layout: {
                // Mapbox Style Specification layout properties
            }
        });
    };

    setAllMapStyling = () => {
        this.map.setStyle(this.props.mapStyle, { "diff": true });
        this.map.on("styledata", () => {
            this.addCircles();
        });

    };
    componentDidUpdate(prevProps, prevState) {
        // Cahnge map Style
        if (prevProps.mapStyle !== this.props.mapStyle) {
            this.setAllMapStyling();
            // this.setState({ cirleColor: prevState.cirleColor === "#FE636D" ? "#ffbc2c" : "#FE636D" }, () => this.setAllMapStyling())
        };
        // Update map center
        if (prevProps.mapViewState !== this.props.mapViewState) {
            this.map.jumpTo(this.props.mapViewState);
        };
    };

    render() {
        return (
            <div className="map" ref={this.mapRef}></div>
        )
    }
}


export default Map;
