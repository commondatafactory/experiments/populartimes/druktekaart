import React from 'react';

class ListItem extends React.Component {
    render() {
        return <tr>
            <td>{this.props.item.name} <span id="small">{this.props.item.address}</span></td>
            <td>{this.props.item.current_popularity}</td>
            <td>{this.props.item.avg_p}</td>
            <td>{this.props.item.current_popularity - this.props.item.avg_p }</td>

            </tr>
    }
}

export default ListItem;
