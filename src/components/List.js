import React from 'react';
import ListItem from './ListItem';
import './List.css';

class List extends React.Component {

    arrayOrder = (listObject) => {
        let sortArray = [];
        for (var item in listObject) {
            sortArray.push([item, listObject[item].properties]);
        }
        sortArray.sort(function (a, b) {
            return b[1].current_popularity - a[1].current_popularity;
        });
        return sortArray.slice(0, 5)

    }

    render() {
        return <div className="ListItem">

            <h3>De top 5 drukke plekken in het kaart beeld:</h3>
            <table>
                <thead>
                    <tr><th>Locatie</th><th>Drukte Huidig</th><th>Drukte gemiddeld op deze tijd</th><th>Verschil</th></tr>
                </thead>
                <tbody>
                    {this.arrayOrder(this.props.arrayMapInfo.info).map(item =>
                        <ListItem
                            key={item[0]}
                            item={item[1]}
                        />
                    )}
                </tbody>
            </table>

        </div>
    }
}

export default List;
