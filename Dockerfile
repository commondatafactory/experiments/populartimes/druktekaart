FROM node:12.16.1-alpine AS build

ENV CI=true

# copy only package.json en yarn.lock to make the dependency fetching step optional.
COPY package.json \
     package-lock.json \
     /app/

COPY node_modules/ /app/node_modules/
COPY public/ /app/public/
COPY src/ /app/src/

RUN ls -la /app/public/*

WORKDIR /app
RUN npm install

RUN npm run build

# Compress gzip js files
RUN find build -name "*.js" | xargs -I % sh -c 'gzip %'

# Copy static docs to alpine-based nginx container.
FROM nginx:alpine AS web

# Add bash
RUN apk add --no-cache bash

# Copy nginx configuration
COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf

COPY --from=build /app/build /usr/share/nginx/html

# Add non-privileged user
RUN adduser -D -u 1001 appuser

# Set ownership nginx.pid and cache folder in order to run nginx as non-root user
RUN touch /var/run/nginx.pid && \
    chown -R appuser /var/run/nginx.pid && \
    chown -R appuser /var/cache/nginx && \
    chown -R appuser /var/log/nginx/

USER appuser

# Start Nginx server
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]
